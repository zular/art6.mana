$(function() {
    if ($('.block-slider').length) {
        window.myFlux = new flux.slider('.block-slider', {
            transitions: ['blocks2'],
            pagination: false
        });
    };
});

$(document).ready(function(){
    $('.slider').bxSlider({
        slideWidth: 370,
        minSlides: 3,
        maxSlides: 3,
        moveSlides: 1,
        pager: false,
        nextSelector: '#arrow-left',
        prevSelector: '#arrow-right',
        auto: true,
        nextText: '',
        prevText: ''
    });

    var maxHeight = 0;

    $(".slider .slide").each(function(){
      if ( $(this).height() > maxHeight ) {
        maxHeight = $(this).height();
      }
    });

    $(".slider .slide").height(maxHeight);

    $('.text-tailor').textTailor({
        fit: false
    });
});

Share = {
    vkontakte: function(purl, ptitle, pimg, text) {
        url  = 'http://vkontakte.ru/share.php?';
        url += 'url='          + encodeURIComponent(purl);
        url += '&title='       + encodeURIComponent(ptitle);
        url += '&description=' + encodeURIComponent(text);
        url += '&image='       + encodeURIComponent(pimg);
        url += '&noparse=true';
        Share.popup(url);
    },
    odnoklassniki: function(purl, text) {
        url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
        url += '&st.comments=' + encodeURIComponent(text);
        url += '&st._surl='    + encodeURIComponent(purl);
        Share.popup(url);
    },
    facebook: function(purl, ptitle, pimg, text) {
        url  = 'http://www.facebook.com/sharer.php?s=100';
        url += '&p[title]='     + encodeURIComponent(ptitle);
        url += '&p[summary]='   + encodeURIComponent(text);
        url += '&p[url]='       + encodeURIComponent(purl);
        url += '&p[images][0]=' + encodeURIComponent(pimg);
        Share.popup(url);
    },
    twitter: function(purl, ptitle) {
        url  = 'http://twitter.com/share?';
        url += 'text='      + encodeURIComponent(ptitle);
        url += '&url='      + encodeURIComponent(purl);
        url += '&counturl=' + encodeURIComponent(purl);
        Share.popup(url);
    },
    google: function(purl, ptitle, pimg, text) {
        url  = 'https://plusone.google.com/_/+1/confirm?hl=en&url=';
        url += 'url='          + encodeURIComponent(purl);
        // url += '&title='       + encodeURIComponent(ptitle);
        // url += '&description=' + encodeURIComponent(text);
        // url += '&imageurl='    + encodeURIComponent(pimg);
        Share.popup(url)
    },

    popup: function(url) {
        window.open(url,'','toolbar=0,status=0,width=626,height=436');
    }
};
